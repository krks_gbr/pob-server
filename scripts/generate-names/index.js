

"use strict";




const randInt = (min, max) => {


    return min + Math.floor( Math.random()*(max-min) );

};

var fs = require('fs');

var names = fs.readFileSync('./names', 'utf-8').split('\n');


let step = 1;

for(let i = 0; i < 1000; i++){

    let name = "";
    let c = 0;

    for( let j = 0; j<5; j++){
        let src = names[randInt(0, names.length)];
        name+=src.substring(c, c+step);
        c+=step;
    }

    console.log(name);

}

