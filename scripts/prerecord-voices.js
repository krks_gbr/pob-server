


import path from 'path';
import mkdirp from 'mkdirp';
import TTS from '../server/lib/TTS';
import Config from '../server/Config';
import _ from 'lodash';


let data = require('../server/data/data.json');
let outDirPath = path.resolve('sounds');
console.log(outDirPath);

let voices = Config.Bots.Voices;
console.log(voices);


let units =
    _.chain(data)
        .map(g => g.units)
        .flatten()
        .map(u => ({id: u.id, text: u.text})).value();



let specs = _.flatten(
        Object.keys(voices).map(botName =>

            units.map(unit => ({


                    text: unit.text,
                    voice: voices[botName],
                    targetDirPath: outDirPath,
                    targetFileName: `${botName}_${unit.id}`

                }))

        )
);



(function sayNext(){
    let spec = specs.pop();
    console.log(`doing ${spec.targetFileName} | ${specs.length} left to export`);
    TTS.say(spec, () => {
        if(specs.length === 0){
            process.exit();
        } else {
            sayNext();
        }
    });
})();

