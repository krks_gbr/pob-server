


import paper from 'paper/dist/paper-full';
import _ from 'lodash';
import TWEEN from 'tween.js';



export default function (canvas, iconPaths, colors, backgroundColor){


    let p = new paper.PaperScope();
    p.setup(canvas);


    window.addEventListener('resize', () => {
        p.view.viewSize = [canvas.width, canvas.height]
    });

    let activeCategory = null;
    let loadedIcons = loadIcons(iconPaths, p.view.center);


    if(backgroundColor){
        let background = new p.Path.Rectangle({
            point: [0, 0],
            size: [canvas.width, canvas.height],
            fillColor: backgroundColor
        });
        background.sendToBack();
    }

    if(colors.length === 1){
        colors = colors.concat(['white', "black"]);
    }

    let colorIndex = 0;

    loadedIcons.then(icons =>
        p.view.onFrame = event => {
            TWEEN.update();
        }
    );


    function loadIcons(iconPaths, middle){
        function loadIcon(path){
            return new Promise((resolve, reject) => {
                p.project.importSVG(path, icon => {
                    icon.position = middle;
                    icon.remove();
                    resolve(icon);
                });
            });
        }

        let paperIcons = {};

        Object.keys(iconPaths).forEach(category => {
            paperIcons[category] = iconPaths[category].map(iconPath => loadIcon(iconPath));
            Promise.all(paperIcons[category]).then(loadedIcons => paperIcons[category] = loadedIcons);
        });



        return new Promise((resolve, reject) => {
            Promise.all(_.flatten(_.values(paperIcons))).then( i => {
                resolve(paperIcons);
            });
        });
    }



    return  {

        setCategory: category => activeCategory = category,
        emit: intensity => {
            p.activate();
            loadedIcons.then(icons => {
                let category = icons[activeCategory];
                let icon = category[Math.floor(Math.random()*category.length)];


                let boundsRect = new p.Path.Rectangle({

                    point: p.view.center,
                    size: [10, 10]

                });


                let bounds =  boundsRect.bounds;


                icon.fitBounds(bounds);
                icon.opacity = 1;
                icon.fillColor = colors[colorIndex];
                icon.insertAbove( p.project.activeLayer.lastChild );

                let startedFade = false;
                let opacityTween = new TWEEN.Tween({opacity: 1})
                    .to({opacity: 0}, 300)
                    .onUpdate(function(){
                        icon.opacity = this.opacity
                    })
                    .onComplete(function(){
                        icon.remove();
                        boundsRect.remove();
                    });

                let scaleTime = mapRange(intensity, 0.1, 0.3, 4.5, 1.5);
                let scaleTween = new TWEEN.Tween({ size: 10  })
                    .to({size: canvas.height*1.4}, 2200)
                    .onUpdate(function(){
                        if(this.size >= canvas.height*1.1 && !startedFade){
                            opacityTween.start();
                            startedFade = true;
                        }
                        bounds.setCenter(p.view.center); bounds.setSize(this.size, this.size); icon.fitBounds(bounds)
                    })
                    // .onComplete(function(){ opacityTween.start() })
                    // .easing(TWEEN.Easing.Linear.None)
                    .start();


                colorIndex = (colorIndex + 1) % colors.length;
            });
        }

    };

    
}





function mapRange(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}