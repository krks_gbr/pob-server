



let SND =  {

    Events :{
       
        PLAY_END: 'onPlayEnded',
        FREQ_DATA: 'onFrequencyData',
        VOLUME_DATA: 'onVolumeData'
       
    },

    Context: new AudioContext(),
    
    //factory
    load: function(url) {
        
            let listeners = {
               
                [SND.Events.PLAY_END]: [],
                [SND.Events.FREQ_DATA]: [],
                [SND.Events.VOLUME_DATA]: []
               
            };
        

            function onError(e){
                console.log("SND ERROR", error);
            }



            function getAvgVolume(frqData){
                //frqData is in decibel scale
                // first convert to linear values, take avg, then convert back to decibel
                var sum = 0;
                frqData.forEach(function(decibel){
                    sum += Math.pow(10, decibel/20);
                });
                sum /= frqData.length;
                return 20*Math.log10(sum);
            }
    


            //HTML5 media element for easier playback control
            let audioElement = new Audio(url);
            audioElement.crossOrigin = "anonymous";
            let source =  SND.Context.createMediaElementSource( audioElement );
    

            audioElement.addEventListener('ended', function(){
                console.log('SND playback ended');
                // notify play ended listeners
                listeners[SND.Events.PLAY_END].forEach(listener => listener());
            });



            let analyser = SND.Context.createAnalyser();
            analyser.smoothingTimeConstant = 0.3;
            analyser.fftSize = 1024;


            let audioProcessor = SND.Context.createScriptProcessor(2048, 1, 1);


            audioProcessor.addEventListener('audioprocess', function () {
                let frequencies = new Uint8Array(analyser.frequencyBinCount);
                analyser.getByteFrequencyData(frequencies);

                //convert to normal array and normalize values
                frequencies = Array.from(frequencies).map( (f) => f/255 );

                //notify frequency data listeners
                listeners[SND.Events.FREQ_DATA].forEach(listener => listener(frequencies));

                // get avg volume and notify listeners
                let avgVolume = getAvgVolume(frequencies);
                listeners[SND.Events.VOLUME_DATA].forEach(listener => listener(avgVolume));
            });



            //connect things
            source.connect(SND.Context.destination);
            source.connect(analyser);
            analyser.connect(audioProcessor);
            // connect to destination, else it isn't called
            audioProcessor.connect(SND.Context.destination);


            function breakDown(){
                console.log('breaking down SND, ', url);
                if(!muted){
                    source.disconnect(SND.Context.destination);
                }
                source.disconnect(analyser);
                analyser.disconnect(audioProcessor);
                audioProcessor.disconnect(SND.Context.destination);
            }

            let muted = false;
            //return instance
            return {

                breakDown: breakDown,

                play: function(){
                    audioElement.play();
                    listeners[SND.Events.PLAY_END].push( breakDown );
                },

                mute: () => {
                    source.disconnect(SND.Context.destination);
                    muted = true;
                },

                pause: function(){
                    audioElement.pause();
                },
                
                paused: function(){
                    return audioElement.paused;
                },
                
                ended: function(){
                    return audioElement.ended;
                },
                
                //// for example: snd.addListener(SND.Events.FREQ_DATA, (data) => console.log(data));
                addListener: function(key, callback){
                    listeners[key].push(callback);
                },
                

                elapsed: function(){
                    return audioElement.currentTime/audioElement.duration;
                }

            }
    }
};

export default SND;