

import IconEmitter from '../icon-emitter';
import SND from '../snd';
import Config from '../config';
import _ from 'lodash';
import {Categories} from '../icon-config';
import IconPaths from '../icon-config';




let Bot = (botName) => {

    console.log("setting up ", botName);
    let canvas = document.createElement('canvas');

    function setCanvasSize(){
        canvas.width = window.innerWidth/4;
        canvas.height = window.innerHeight;
    }

    setCanvasSize();
    window.addEventListener('resize', setCanvasSize, false);


    document.getElementById('wrapper').appendChild(canvas);

    console.log("creating icon Emitter");
    let iconEmitter = IconEmitter( canvas, IconPaths, Config.Colors[botName] );
    iconEmitter.setCategory(Categories.ACTION);


    console.log('');
    
    let muted = false;

    let snd = null;

    return {

        toggleMute: () => muted = !muted,
        stop: () => {
            if(snd){
                console.log('shutup bot!');
                snd.pause();
                snd.breakDown();
            }
        },
        speak: (soundPath, callback) => {
            // iconEmitter.activate();
            snd = SND.load(soundPath);

            let throttled = _.throttle(volume => {
                // console.log(volume);
                if(volume > Config.VOLUME_THRESHOLD){
                    iconEmitter.emit(volume);
                }
            }, Config.EMITTER_THROTTLE);

            snd.addListener(SND.Events.VOLUME_DATA, throttled);

            if(callback){
                snd.addListener(SND.Events.PLAY_END, callback);
            }
            
            if(muted){
                snd.mute();
            }
            
            snd.play();
        }
        
    }
    
};


export default Bot;

