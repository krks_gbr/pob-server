import {BotNames, BotNameList} from '../../../../shared/config';
import IconPaths from '../icon-config';
import _ from 'lodash';




let Icons = {
    [BotNames.GIBS]: IconPaths.SOCIAL,
    [BotNames.TEOL]: IconPaths.HARDWARE,
    [BotNames.VOLLH]: IconPaths.PLACES,
    [BotNames.AUMNA]: IconPaths.NOTIFICATION,
    getIconCategoryNameForBotName(botName){
        let categoryNames = Object.keys(IconPaths);
        let categoryIndex = _.values(IconPaths).findIndex(category => category === this[botName] );
        return categoryNames[categoryIndex];
    }
};


let Colors = {

    [BotNames.GIBS]: ["#EF4338"],
    [BotNames.TEOL]: ["#FFC20E", "#352F8B"],
    [BotNames.VOLLH]: ["#EE2749", "#34C1D6"],
    [BotNames.AUMNA]: ["#2BB673"]

};




export default {

    BotNames: BotNames,
    BotNameList: BotNameList,

    Icons: Icons,
    Colors: Colors,
    
    VOLUME_THRESHOLD: 0.14,
    EMITTER_THROTTLE: 115,


}



