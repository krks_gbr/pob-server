



import $ from 'jquery';
import '../../reset.scss';
import './style.scss';
import Bot from './bot';
import Config from './config';
import createPauseScreen from './../pauseScreen';
import socket from '../../socket';
import axios from 'axios';
import {VoiceMapping} from '../../../shared/config';


const {TTS_HOST} = process.env;

document.body.style.cursor = 'none';


window.onload = function(){

    console.log(process.env);
    let $pauseContainer = $('<div>').appendTo('#wrapper');
    $pauseContainer.addClass('hidden');
    
    Array(4).fill().map(() => createPauseScreen({$container: $pauseContainer}));


    let bots = {};

    Config.BotNameList.forEach(name => bots[name] = Bot(name));


    socket.on('connection', () => {
        console.log('socket connected');
    });


    let currentBot = null;
    socket.on('message', message => {

        let voice = VoiceMapping[message.sender];
        getSoundPath(message, voice).then(soundPath =>{
            console.log(soundPath);
            currentBot = bots[message.sender];
            bots[message.sender].speak(soundPath, () => {

                if(socket.connected){
                    socket.emit('spoken');
                }

            });

        });


    });


    socket.on('pause', () => {
        console.log('received pause event');
        if(currentBot){
            currentBot.stop();
        }
        $pauseContainer.removeClass('hidden');

    });

    socket.on('resume', () => {
        console.log('received resume event');
        $pauseContainer.addClass('hidden');
    });

};


function getSoundPath(message, voice){
    console.log(message, voice);
    if(message.AIML){
        return new Promise((resolve, reject) => {

            axios.post(`${TTS_HOST}/tts`, {text: message.content, voice: voice})
                .then(res => resolve(`${TTS_HOST}/${res.data}`))
                .catch(e => {
                    socket.emit('down');
                    socket.emit('spoken');
                });
            ;

        });
    } else {
        return Promise.resolve(`/sounds/${message.sender}_${message.unitID}.mp3`);
    }
}
