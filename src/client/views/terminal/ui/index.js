

import $ from 'jquery';
import Header from './header';



export default (function ($root){


    let header = Header();
    header.$.appendTo($root);


    let $contentWrapper = $('<div>').attr('id', 'contentWrapper').addClass().appendTo($root).addClass('row');
    let $content = $('<div>').attr('id', 'content').appendTo($contentWrapper);
    $('<div>').attr('id', 'gradient').appendTo($contentWrapper);




    const displayMessage = (msg, cb) => {

        let $messageContainer = $('<div>').addClass('messageContainer');
        $('<div>').addClass('senderName').text(msg.sender).appendTo($messageContainer);
        $('<div>').addClass('messageContent').text(msg.content).appendTo($messageContainer);
        $messageContainer.appendTo($content);
        
        if(msg.fromHuman){
            $messageContainer.addClass('highlight');
            setTimeout(() => $messageContainer.removeClass('highlight'), 1000);
        }

    };

    const fixScrollBottom = () => {
        let el = document.getElementById('contentWrapper');
        let lastMessageContainerHeight = $('.messageContainer').last()[0].clientHeight;
        let precision = 100;

        let isScrolledToBottom  = el.scrollTop + el.clientHeight >= el.scrollHeight - (lastMessageContainerHeight + precision);
        if(isScrolledToBottom){
            el.scrollTop = el.scrollHeight
        }
    };
    
    return {
        header: header,
        displayMessage: msg => {
            if($('.messageContainer').length > 30){
                $('.messageContainer').first().remove();
            }
            displayMessage(msg);
            fixScrollBottom();
        }
    }
    
})($('#app'));