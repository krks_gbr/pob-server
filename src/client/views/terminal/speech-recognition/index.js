

const Recognition = function() {


    
    console.log('initializing recognition');
    let recognition = new  webkitSpeechRecognition();
    recognition.continuous = true;
    recognition.interimResults = true;

    let onProgress = null;
    let resultListener = null;
    let onBeforeSend = null;
    let resultIndexes = [];

    let paused = false;





    function handleResult(text, resultIndex){
            if(resultIndexes.includes(resultIndex)){
                return;
            }

            resultIndexes.push(resultIndex);
            resultListener(text);
    }

    recognition.onend = event => {
        console.log('resuming recognition');
        recognition.start();
    };



    return {
        
        
        pause: () => {
           paused = true;
        },
        
        resume: () => {
           paused = false;
        },

        onResult: listen => {
            resultListener = listen;
        },
        
        onProgress: listen => {
            onProgress = listen;
        },
        
        

        init: () => {

            recognition.start();


            let timeout = null;
            recognition.onresult = function(event) {

                if(!paused){
                    if(resultIndexes.includes(event.resultIndex)){
                        return;
                    }


                    console.log(event);
                    let transcript = null;
                    let interimTranscript = null;
                    for (let i = event.resultIndex; i < event.results.length; ++i) {
                        if (event.results[i].isFinal) {
                            transcript = event.results[i][0].transcript;
                        } else {
                            interimTranscript = event.results[i][0].transcript;
                            onProgress(event.results[i][0].transcript);
                        }
                    }

                    if(timeout){
                        clearTimeout(timeout);
                    }

                    timeout = setTimeout(() => {
                        handleResult(interimTranscript, event.resultIndex);
                        transcript = null;
                        console.log('submitted interim transcript', interimTranscript);
                    }, 2500);


                    if(transcript){
                        clearTimeout(timeout);
                        handleResult(transcript, event.resultIndex);
                        console.log('submitted final transcript', transcript);
                    }
                }


            };

        }
    }
    
};

Recognition.States = {

    STANDBY: 'standby',
    PROGRESS: 'in progress',
    RESULT: 'result'
    
};

export default Recognition;
let States = Recognition.States;
export {States};