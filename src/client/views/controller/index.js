



import io from 'socket.io-client'
import './style.scss';
import '../../reset.scss';
import $ from 'jquery';
import socket from '../../socket';





socket.on('connect', () => {
    console.log('controller connected');

});

socket.on('pause', () =>{
    $('#onBtn').removeClass('active');
    $('#offBtn').addClass('active');
});

socket.on('resume', () => {
    $('#offBtn').removeClass('active');
    $('#onBtn').addClass('active');
});


$('<div>')
    .addClass('button')
    .addClass('active')
    .attr('id', 'onBtn')
    .append($('<span>').addClass('btn-label').text('on'))
    .appendTo('#wrapper')
    .on('click', () =>{
        onButtonClicked();
        socket.emit('resume');

    });

$('<div>')
    .addClass('button')
    .attr('id', 'offBtn')
    .append($('<span>').addClass('btn-label').text('off'))
    .appendTo('#wrapper')
    .on('click', () =>{
        onButtonClicked();
        socket.emit('pause');
    });



function onButtonClicked(){
    $('#wrapper').addClass('highlight');
    setTimeout(() => $('#wrapper').removeClass('highlight'), 2000);
}




