
import io from 'socket.io-client';

export default io(`${window.__host__}${window.__nsp__}`, {forceNew: true});