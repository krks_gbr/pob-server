import path from "path";
import _ from "lodash";
import {BotNames, BotNameList} from '../shared/config';



let TTS_VOICES =  {
        LEE: "Lee",
        SAMANTHA: "Samantha",
        DANIEL: "Daniel",
        TOM: "Tom",
        TESSA: "Tessa"
};


let VoiceMapping = {

    Voices: {
        [BotNames.TEOL]: TTS_VOICES.SAMANTHA,
        [BotNames.VOLLH]: TTS_VOICES.LEE,
        [BotNames.GIBS]: TTS_VOICES.DANIEL,
        [BotNames.AUMNA]: TTS_VOICES.TESSA
    }

};

let ROOT_PATH =  path.dirname(require.main.filename);

let ENV_PATH = path.resolve(
    ROOT_PATH,
    process.env.NODE_ENV === 'production' ? 'dist' : 'src',
);

let Config = {
    BotNameList: BotNameList,
    Bots: {
        ...BotNames,
        ...VoiceMapping,
    },
    STORAGE_DIR:    path.join(ROOT_PATH, "storage"),
    TEMPLATES_DIR:  path.join(ENV_PATH, "server", "templates"),
    DATA:           path.join(ENV_PATH, "server", "data", "data.json"),
    PUBLIC_DIR:     path.join(ROOT_PATH, "dist", "client")
};


export default Config;