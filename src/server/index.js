
import http from 'http';
import Config from './config';
import Express from 'express';
import morgan from 'morgan';



export default ({address, port}) => {

    var app = Express();
    var server = http.createServer(app);

    app.set('views', Config.TEMPLATES_DIR);
    app.set('view engine', 'pug');

    // app.use(morgan('dev'));
    app.use( Express.static( Config.PUBLIC_DIR ));
    app.use( Express.static( Config.STORAGE_DIR ) );


    app.locals.host = `http://${address}:${port}`;

    app.get('/controller', function(req, res){
        res.locals.nsp = '/controller';
        res.render('controller');
    });

    app.get('/animation', function(req, res){
        res.locals.nsp = '/animation';
        res.render('animation');
    });


    app.get('/terminal', function(req, res){
        res.locals.nsp = '/terminal';
        res.render('terminal');
    });

    app.get('/network-test', function(req, res){
        res.render('network-test');
    });


    server.listen(port, function(){
        console.log('parliament running on ' + address + ":" + port);
    });
    
    return server;
};



