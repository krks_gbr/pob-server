
var fs = require('fs');

module.exports = function(){

    var file = 'log.json';
    return {

        log: function(data){
            var prevData = JSON.parse(fs.readFileSync(file));
            data['id'] = prevData.length;
            prevData.push(data);
            fs.writeFileSync(file, JSON.stringify(prevData, null, 4));
        }

    }

};