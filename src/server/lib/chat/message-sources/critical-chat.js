



import fs from 'fs';
import Config from '../../../config';
import _ from 'lodash';
import Message from '../../../../shared/message';
import {getNextBot} from '../helpers';



const data = JSON.parse(fs.readFileSync( Config.DATA ));
export default () => {

    
    return {
        
        reply: (message, callback) => {
            let related = findRelated(message.unitID);
            let unit = related.unit;
            unit.locked = true;

            setTimeout(() => {
                unit.locked = false;
            }, 5*60*1000);

            let sender = message.recipient;
            let recipient = getNextBot(sender);
            let reply = Message(sender, unit.text, recipient, unit.id);

            if(related.random){
                setTimeout(() => callback(reply), 1000);
                return;
            }

            callback(reply);
        }
    }

}

let chain = 0;
function findRelated(unitID){

    if(unitID){

        let allUnits = getAllUnits();
        let unit  =  _.find(allUnits, {id: unitID});

        if(unit.related.length > 0){
            let candidates = unit.related   .map(rel => _.find(allUnits, {id: rel.unitID}))
                                            .filter(unit => !unit.locked);

            if(candidates.length > 0){
                chain++;
                return {
                    unit: candidates[Math.floor(Math.random()*candidates.length)],
                    random: false
                };
            }

        }
        
    }

    console.log('could not find related unit. returning random unit. chain:', chain);
    chain = 0;
    return {
        unit: pickRandomUnit(),
        random: true,
    }


}

function getAllUnits(){
    return _.flatten(data.map(group => group.units));
}

function pickRandomUnit(){
    let units = getAllUnits().filter(unit => unit.related.length > 5);
    return units[Math.floor(Math.random()*units.length)];
}

