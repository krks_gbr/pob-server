


import {cleanMessage} from '../helpers';
import {getNextBot} from '../helpers';
import Message from '../../../../shared/message';
import axios from 'axios';
import Config from '../../../Config';



export default botsIO => {


    let listener = null;
    
    let botSocketConnected = (function connect() {

        return new Promise((resolve, reject) => {
            botsIO.on('connection', socket => {
                console.log('connected to AIML bots');
                resolve(socket);
                socket.on('disconnect', () => {
                    console.log('disconnected from AIML bots');
                    botSocketConnected = connect()
                });
            });
        });

    })();
    
    botSocketConnected.then(socket => socket.on('message', message => {
        let cleanedMessage = cleanMessage(message.content);
        let m = Message(message.sender, cleanedMessage, getNextBot(message.sender), null);
        m.AIML = true;
        listener(m);
    }));

    
    return {

        reply: (message, callback) => {

            listener = callback;
            botSocketConnected.then(socket => {
                if(message.content.includes('@Human')){
                    message.content = message.content.replace('@Human', "");
                }
                socket.emit('message', message);
            });

        }
        
    }
    
    
    
    
}