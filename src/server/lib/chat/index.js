

import CriticalChat from './message-sources/critical-chat';
import AIMLChat from './message-sources/aiml-chat';
import Message from '../../../shared/message';
import Config from '../../Config';
import {getNextBot} from './helpers';




let Chat = function(botsIO){
    

    let userMessages = [];
    let STYLE = Chat.STYLES.CRITICAL;
    let paused = false;
    let onResume = null;
    let count = 0;


    let MessageSources = {

        [Chat.STYLES.CRITICAL]: CriticalChat(),
        [Chat.STYLES.AIML]: AIMLChat(botsIO),

    };

    
    let currentMessage = Message(Config.BotNameList[0], 'Hello', Config.BotNameList[1]);

    let keepAIMLCount = 0;

    let next = onMsgCallback => {

        let toUser = false;
        let source = MessageSources[STYLE];
        if(userMessages.length > 0 ){
            currentMessage = userMessages.shift();
            source = MessageSources[Chat.STYLES.AIML];
            toUser = true;
        }

        if(keepAIMLCount > 0){
            source = MessageSources[Chat.STYLES.AIML];
            keepAIMLCount --;
        }

        currentMessage.recipient =  currentMessage.recipient || getNextBot(currentMessage.sender);

        source.reply(currentMessage, reply => {
            currentMessage = reply;
            if(toUser){
                reply.content = "@Human: " + reply.content;
            }

            onMsgCallback(currentMessage);

        });

        count++;

    };


    let resume = () => {
            console.log("resuming chat");
            paused = false;
            if(onResume){
                onResume();
                onResume = null;
            }
    };



    return {

        handleUserMessage: message => {
            keepAIMLCount = 7;
            userMessages.push(message)
        },

        switchStyle: () => {
            if(STYLE === Chat.STYLES.AIML){
                STYLE = Chat.STYLES.CRITICAL;
            } else {
                STYLE = Chat.STYLES.AIML;
            }
            console.log('switched mode to', STYLE);
            
        },

        setStyle: style => {
            STYLE=style;
        },

        getStyle: ()  => STYLE,
        next: next,
        count: () => count,


    }
};




Chat.STYLES = {
    AIML: 'aiml',
    CRITICAL: 'critical'
};

export default Chat;