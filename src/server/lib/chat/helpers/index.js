
import Config from '../../../Config';


/**
 * Picks a bot at random to deliver the message to
 * @function getNextBot
 * @returns {string}
 * @param {string} prevBot
 */

function getNextBot(prevBot) {
    let nextBots = Config.BotNameList.filter(name => name != prevBot);
    return nextBots[Math.floor(Math.random() * nextBots.length)];
}



function normalizeWhiteSpace(str){
    return str.replace(/\s\s+/g, " ");
}


//http://stackoverflow.com/questions/11761563/javascript-regexp-for-splitting-text-into-sentences-and-keeping-the-delimiter
function clampMessageLength(msg, n){
    var result = msg.match( /[^\.!\?]+[\.!\?]+/g );
    if(result && result.length > n){
        return result.slice(0, n).join(" ");
    }

    return msg;

}

function cleanMessage(str){
    // the number of sentences the bots exchange tend to rise indefinitely, so we need to limit it
    str = clampMessageLength( str, Math.floor(Math.random() * 3) + 1 );
    str = normalizeWhiteSpace(str);
    str = str.replace('<br/>', "");
    return str;
}


export {getNextBot};
export {cleanMessage};


