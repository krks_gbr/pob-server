

import Chat from '../chat'


export default io => {

    //namespaces for socket connections
    let NSPS = {
        BOTS: '/bots',
        ANIMATION_VIEW: '/animation',
        TERMINAL_VIEW: '/terminal',
        CONTROLLER: '/controller'
    };

    //initialize namespaces
    let botsIO = io.of(NSPS.BOTS);
    let animationView = io.of(NSPS.ANIMATION_VIEW);
    let terminalView = io.of(NSPS.TERMINAL_VIEW);
    let controller = io.of(NSPS.CONTROLLER);
    
    
    let views = [animationView, terminalView, controller];


    //keep track of clients in view namespaces
    let clientCounts = {

        [NSPS.BOTS]: 0,
        [NSPS.ANIMATION_VIEW]: 0,
        [NSPS.TERMINAL_VIEW]: 0,
        [NSPS.CONTROLLER]: 0,

    };

    let chat = Chat(botsIO);

    let switches = [15,5, 5,3, 10,2, 1,1,1,1, 5,3, 10,10, 5,2, 2,2,2,2, 7,5,5,3, 3,3,3,3, 3,2, 2,5 ];
    // let switches = [3,3, 3,3, 3,3, 1,1,1,1, 3,3, 3,3, 3,3, 2,2,2,2, 3,3,3,3, 3,3,3,3, 3,3, 2,3 ];
    let switchIndex = 0;

    let idle = false;

    views.forEach(view => {

        view.on('connection', socket =>  {
            clientCounts[socket.nsp.name] ++;

            socket.on('disconnect', () => {
                clientCounts[socket.nsp.name] --;
                socket.removeAllListeners();
                console.log('lost connection with ', socket.nsp.name);
                console.log('clientCounts', JSON.stringify(clientCounts, null, 4));
            });


            console.log('new client at ', socket.nsp.name);
            console.log('clientCounts', JSON.stringify(clientCounts, null, 4));

        });

    });



    let handleMessage = msg => {
        console.log("");
        animationView.emit('message', msg);
        terminalView.emit('message', msg );
    };

    animationView.on('connection', socket => {


        if(clientCounts[NSPS.ANIMATION_VIEW] === 1){
            if(!idle){
                chat.next(handleMessage);
            }
        }

        socket.on('spoken', () => {

            console.log('switching in', switches[switchIndex] - (chat.count() % switches[switchIndex]));
            if(chat.count() % switches[switchIndex] === 0){
                chat.switchStyle();
                switchIndex = (switchIndex+1) % switches.length;
            }

            if(!idle){
                chat.next(handleMessage);
            }
        });

        socket.on('down', () => chat.switchStyle());


        socket.on('disconnect', () => console.log('animation disconnected'));



    });



    terminalView.on('connection', socket => socket.on('message', chat.handleUserMessage));


    controller.on('connection', socket => {


        socket.on('pause', () => {
            console.log('broadcasting pause');
            idle = true;
            animationView.emit('pause');
            terminalView.emit('pause');
        });

        socket.on('resume', () => {
            idle = false;
            console.log('broadcasting resume');
            animationView.emit('resume');
            terminalView.emit('resume');
            chat.next(handleMessage);
        });

    });
    
}
