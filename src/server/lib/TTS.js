

import childProcess from 'child_process';
import path from 'path';

const spawn = childProcess.spawn;

export default  {
    say: ({text, voice, targetDirPath, targetFileName, loud}, callback) => {

        targetFileName = targetFileName  + ".wav";
        let outFilePath = path.join(targetDirPath, targetFileName);

        let commands = [    '-v', voice,
                            text
        ];
        
        if(!loud){
            commands = commands.concat( [    '--file-format=WAVE','',
                                             '--data-format=LEF32@32000',
                                             '-o', outFilePath
                                        ] )
        }

        let say = spawn('say', commands);

        say.stderr.once('data', data => { throw new Error(data) });

        say.stdout.on('data', data => console.log('out', data));

        say.on('exit', code => {
            if(callback){
                if(!loud){
                    callback(targetFileName);
                } else {
                    callback();
                }
            }
        });
    }
};