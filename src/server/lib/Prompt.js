


module.exports = function(){

    process.stdin.resume();
    process.stdin.setEncoding('utf8');
    
    return {
        
        onInput: function(cb){
            process.stdin.resume();
            process.stdin.on('data', cb);
        }
        
    }
    
};